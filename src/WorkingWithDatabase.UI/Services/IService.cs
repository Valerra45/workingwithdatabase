﻿using WorkingWithDatabase.Core.Domain;

namespace WorkingWithDatabase.UI.Services
{
    public interface IService
    {
        void Show();

        void Add();
    }
}
