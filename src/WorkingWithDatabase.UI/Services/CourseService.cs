﻿using System;
using System.Threading;
using WorkingWithDatabase.Core.Abstractions;
using WorkingWithDatabase.Core.Domain;

namespace WorkingWithDatabase.UI.Services
{
    public class CourseService : IService
    {
        private readonly IRepository _repository;

        public CourseService(IRepository repository)
        {
            _repository = repository;
        }

        public void Add()
        {
            Console.WriteLine("Add Course");

            Console.Write("Name: ");
            var name = Console.ReadLine();

            Console.Write("Description: ");
            var description = Console.ReadLine();

            _repository.Add(new Course()
            {
                Name = name,
                Description = description
            });

            Console.WriteLine("Add...");
            Thread.Sleep(1000);
        }

        public void Show()
        {
            var entitys = _repository.GetAll();

            foreach (var entity in entitys)
            {
                Console.WriteLine(entity.ToString());
            }
        }
    }
}
