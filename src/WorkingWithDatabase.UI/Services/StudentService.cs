﻿using System;
using System.Threading;
using WorkingWithDatabase.Core.Abstractions;
using WorkingWithDatabase.Core.Domain;

namespace WorkingWithDatabase.UI.Services
{
    public class StudentService : IService
    {
        private readonly IRepository _repository;

        public StudentService(IRepository repository)
        {
            _repository = repository;
        }

        public void Add()
        {
            Console.WriteLine("Add Student.");

            Console.Write("First Name: ");
            var firstName = Console.ReadLine();

            Console.Write("Middle Name: ");
            var middleName = Console.ReadLine();

            Console.Write("Second Name: ");
            var secondName = Console.ReadLine();

            _repository.Add(new Student()
            {
                FirstName = firstName,
                MiddleName = middleName,
                SecondName = secondName
            });

            Console.WriteLine("Add...");
            Thread.Sleep(1000);
        }

        public void Show()
        {
            var entitys = _repository.GetAll();

            foreach (var entity in entitys)
            {
                Console.WriteLine(entity.ToString());

                (entity as Student).Courses.ForEach(x => Console.WriteLine($" {x.Name}"));
            }
        }
    }
}
