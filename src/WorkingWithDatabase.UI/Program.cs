﻿using System;
using WorkingWithDatabase.DataAccess.Repositories;
using WorkingWithDatabase.UI.Menus;

namespace WorkingWithDatabase.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            const string cs = @"Host = localhost; Port = 5432; Database = otus234; User Id = postgres; Password = 111;";

            var studentRepository = new StudentRepository(cs);
            var teacherRepository = new TeacherRepository(cs);
            var courseRepository = new CourseRepository(cs);

            new MainMenu(studentRepository, teacherRepository, courseRepository).Show();
        }
    }
}
