﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkingWithDatabase.Core.Abstractions;
using WorkingWithDatabase.Core.Domain;
using WorkingWithDatabase.UI.Services;

namespace WorkingWithDatabase.UI.Menus
{
    public class MainMenu
    {
        private static int _arrow = 0;

        private static string[] _menu =
        {
            "Courses",
            "Students",
            "Teachers",
            "Exit"
        };

        private readonly IRepository _studentRepository;
        private readonly IRepository _teacherRepository;
        private readonly IRepository _courseRepository;

        public MainMenu(IRepository studentRepository,
            IRepository teacherRepository,
            IRepository courseRepository)
        {
            _studentRepository = studentRepository;
            _teacherRepository = teacherRepository;
            _courseRepository = courseRepository;
        }

        public void Show()
        {
            while (true)
            {
                Console.Clear();

                if (_arrow < (int)Menu.Courses)
                {
                    _arrow = (int)Menu.Exit;
                }
                else if (_arrow > (int)Menu.Exit)
                {
                    _arrow = (int)Menu.Courses;
                }

                for (int i = 0; i < _menu.Length; i++)
                {
                    Console.WriteLine($"{(_arrow == i ? ">>" : "  ")} {_menu[i]}");
                }

                ConsoleKey key = Console.ReadKey().Key;

                if (key == ConsoleKey.UpArrow)
                {
                    _arrow--;
                }
                else if (key == ConsoleKey.DownArrow)
                {
                    _arrow++;
                }
                else if (key == ConsoleKey.Enter)
                {
                    switch ((Menu)_arrow)
                    {
                        case Menu.Courses:
                            Console.Clear();
                            new SubMenu(new CourseService(_courseRepository)).Show();
                            break;
                        case Menu.Students:
                            Console.Clear();
                            new SubMenu(new StudentService(_studentRepository)).Show();
                            break;
                        case Menu.Teachers:
                            Console.Clear();
                            new SubMenu(new TeacherService(_teacherRepository)).Show();
                            break;
                        case Menu.Exit:
                            Console.Clear();
                            return;
                    }
                }
            }
        }

        private enum Menu
        {
            Courses,
            Students,
            Teachers,
            Exit
        }
    }
}
