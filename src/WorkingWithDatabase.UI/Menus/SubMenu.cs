﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkingWithDatabase.Core.Abstractions;
using WorkingWithDatabase.Core.Domain;
using WorkingWithDatabase.UI.Services;

namespace WorkingWithDatabase.UI.Menus
{
    public class SubMenu
    {
        private static int _arrow = 0;

        private static string[] _menu =
        {
            "Show",
            "Add",
            "Exit"
        };

        private readonly IService _service;

        public SubMenu(IService service)
        {
            _service = service;
        }

        public void Show()
        {
            while (true)
            {
                Console.Clear();

                if (_arrow < (int)Menu.Show)
                {
                    _arrow = (int)Menu.Exit;
                }
                else if (_arrow > (int)Menu.Exit)
                {
                    _arrow = (int)Menu.Show;
                }

                for (int i = 0; i < _menu.Length; i++)
                {
                    Console.WriteLine($"{(_arrow == i ? ">>" : "  ")} {_menu[i]}");
                }

                ConsoleKey key = Console.ReadKey().Key;

                if (key == ConsoleKey.UpArrow)
                {
                    _arrow--;
                }
                else if (key == ConsoleKey.DownArrow)
                {
                    _arrow++;
                }
                else if (key == ConsoleKey.Enter)
                {
                    switch ((Menu)_arrow)
                    {
                        case Menu.Show:
                            Console.Clear();

                            _service.Show();
         
                            Console.WriteLine();
                            Console.WriteLine("Press any key to continue...");
                            Console.ReadKey();
                            break;
                        case Menu.Add:
                            Console.Clear();

                            _service.Add();

                            break;
                        case Menu.Exit:
                            Console.Clear();
                            return;
                    }
                }
            }
        }

        private enum Menu
        {
            Show,
            Add,
            Exit
        }
    }
}
