﻿namespace WorkingWithDatabase.Core.Domain
{
    public class Course : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public Teacher Teacher { get; set; }

        public override string ToString()
        {
            return $"{Name}, {Teacher}";
        }
    }
}
