﻿namespace WorkingWithDatabase.Core.Domain
{
    public class Teacher : BaseEntity
    {
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string SecondName { get; set; }

        public override string ToString()
        {
            return $"{FirstName} {MiddleName} {SecondName}";
        }
    }
}
