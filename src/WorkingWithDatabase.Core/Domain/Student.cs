﻿using System.Collections.Generic;

namespace WorkingWithDatabase.Core.Domain
{
    public class Student : BaseEntity
    {
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string SecondName { get; set; }

        public List<Course> Courses { get; set; }

        public override string ToString()
        {
            return $"{FirstName} {MiddleName} {SecondName}";
        }
    }
}
