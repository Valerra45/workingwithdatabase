﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkingWithDatabase.Core.Domain;

namespace WorkingWithDatabase.Core.Abstractions
{
    public interface IRepository
    {
        IEnumerable<BaseEntity> GetAll();

        void Add(BaseEntity obj);
    }
}
