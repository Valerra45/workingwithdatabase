﻿using Dapper;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkingWithDatabase.Core.Abstractions;
using WorkingWithDatabase.Core.Domain;

namespace WorkingWithDatabase.DataAccess.Repositories
{
    public class CourseRepository : IRepository
    {
        private readonly string _cs;

        public CourseRepository(string cs)
        {
            _cs = cs;
        }

        public void Add(BaseEntity obj)
        {
            using (var connection = new NpgsqlConnection(_cs))
            {
                var sql = "INSERT INTO сourses (Name, Description) VALUES (@name, @descriptiob)";

                var course = (Course)obj;

                connection.Open();

                connection.Execute(sql, new { name = course.Name, descriptiob = course.Description });
            }
        }

        public IEnumerable<BaseEntity> GetAll()
        {
            using (var connection = new NpgsqlConnection(_cs))
            {
                var sql = "SELECT * FROM сourses LEFT JOIN teachers ON сourses.TeacherId = teachers.Id";

                connection.Open();

                return connection.Query<Course, Teacher, Course>(sql, (course, teacher) =>
                    {
                        course.Teacher = teacher;
                        return course;
                    },
                    splitOn: "Id")
                    .ToList();
            }
        }
    }
}
