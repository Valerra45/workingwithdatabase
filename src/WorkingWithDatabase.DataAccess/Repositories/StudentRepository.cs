﻿using Dapper;
using Npgsql;
using System.Collections.Generic;
using System.Linq;
using WorkingWithDatabase.Core.Abstractions;
using WorkingWithDatabase.Core.Domain;

namespace WorkingWithDatabase.DataAccess.Repositories
{
    public class StudentRepository : IRepository
    {
        private readonly string _cs;

        public StudentRepository(string cs)
        {
            _cs = cs;
        }

        public void Add(BaseEntity obj)
        {
            using (var connection = new NpgsqlConnection(_cs))
            {
                var sql = "INSERT INTO students (FirstName, MiddleName, SecondName) VALUES (@firstName, @middleName, @secondName)";

                var student = (Student)obj;

                connection.Open();

                connection.Execute(sql, new { firstName = student.FirstName, middleName = student.MiddleName, secondName = student.SecondName });
            }
        }

        public IEnumerable<BaseEntity> GetAll()
        {
            using (var connection = new NpgsqlConnection(_cs))
            {
                var sql = "SELECT * FROM students INNER JOIN сourse_student ON сourse_student.StudentId = students.Id " +
                    "INNER JOIN сourses ON сourses.Id = сourse_student.CourseId";

                connection.Open();

                return connection.Query<Student, Course, Student>(sql, (student, course) =>
                    {
                        student.Courses = student.Courses ?? new List<Course>();
                        student.Courses.Add(course);
                        return student;
                    })
                    .GroupBy(x => x.Id)
                    .Select(g =>
                    {
                        var combinedStudent = g.First();
                        combinedStudent.Courses = g.Select(x => x.Courses.Single()).ToList();
                        return combinedStudent;
                    });
               
            }
        }
    }
}
