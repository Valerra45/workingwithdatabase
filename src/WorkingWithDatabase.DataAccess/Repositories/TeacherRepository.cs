﻿using Dapper;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkingWithDatabase.Core.Abstractions;
using WorkingWithDatabase.Core.Domain;

namespace WorkingWithDatabase.DataAccess.Repositories
{
    public class TeacherRepository : IRepository
    {
        private readonly string _cs;

        public TeacherRepository(string cs)
        {
            _cs = cs;
        }

        public void Add(BaseEntity obj)
        {
            using (var connection = new NpgsqlConnection(_cs))
            {
                var sql = "INSERT INTO teachers (FirstName, MiddleName, SecondName) VALUES (@firstName, @middleName, @secondName)";

                var teacher = (Teacher)obj;

                connection.Open();

                connection.Execute(sql, new { firstName = teacher.FirstName, middleName = teacher.MiddleName, secondName = teacher.SecondName });
            }
        }

        public IEnumerable<BaseEntity> GetAll()
        {
            using (var connection = new NpgsqlConnection(_cs))
            {
                var sql = "SELECT * FROM teachers";
                connection.Open();
                return connection.Query<Teacher>(sql)
                    .ToList();
            }
        }
    }
}
